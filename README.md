# voxel tester

## voxeljs

Learn more at http://voxeljs.com

## Start
clone this project

```
cd voxel-hello-world
npm install
```

Then run the start script:

```
npm start
```

open browser
```
localhost:8080
```

## Usage
move : `A` `W` `S` `D` `Space` `Shift`

angle : `mouse drag`

change camera view : `R`



