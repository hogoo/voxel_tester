var createGame = require('voxel-engine')
//var highlight = require('voxel-highlight')
var player = require('voxel-player')
var voxel = require('voxel')
var extend = require('extend')
var fly = require('voxel-fly')
//var walk = require('voxel-walk')
var binary_map = new Array
var limit_x = 0
var limit_y = 0
var limit_z = 0

module.exports = function(opts, setup) {
  // setup the game and add some trees
  $("#btn_start").on("click",function(){
    $("#btn_start").prop("disabled", true)
    var binary_map = getData()

    setup = setup || defaultSetup
    var limit_color = 1 //16777215
    var max_value = -1
    for(var i=0;i<binary_map.length;i++){
      if(parseInt(binary_map[i]) > max_value)
        max_value = parseInt(binary_map[i])
    }
    var color_gap = 1.0 * limit_color / max_value

    var colors = new Array
    colors[0] = "#ffffff"
    for(var i=0;i<max_value;i++){
      var rgb = HSVtoRGB(color_gap * (i+1), 1 , 1 )
      var color_value = "#" + to_hex_string(rgb.r) + to_hex_string(rgb.g) + to_hex_string(rgb.b)
      colors[i+1] = color_value
      $("#color").append((i+1) + " : <span style='color:"+colors[i+1]+"'>" + colors[i+1] + "</span>\t")
    }

    var defaults = {
      generate: function(x,y,z) {
        if(y==-1) // set 바닥
          return 1
        return 0 // sphere world
      },
      chunkSize: 32,
      chunkDistance: parseInt($("#chunk_distance").val()),
      materials: colors,
      materialFlatColor: true,
      worldOrigin: [0, 0, 0],
      controls: { discreteFire: true },
    }
    opts = extend({}, defaults, opts || {})


    var game = createGame(opts)
    var container = opts.container || document.body
    window.game = game // for debugging
    game.appendTo(container)

    if (game.notCapable()) return game

    var createPlayer = player(game)
    var avatar = createPlayer('player.png')
    setBlocks(game, binary_map)

    avatar.possess()
    avatar.yaw.position.set(limit_x, 0, limit_z)
    //avatar.yaw.position.set(0, 0, 0)
    avatar.toggle()
    avatar.height=0.1
    setup(game, avatar)
  })
 }

function getData() {
  var input_data = $("#map").val()
  var arr = input_data.replace(/\n/g, " ").split(" ");
  limit_z = parseInt(arr[0])
  limit_y = parseInt(arr[1])
  limit_x = parseInt(arr[2])
  var binary_map = arr.splice(3, limit_x * limit_y * limit_z)

  for(var i=0;i<binary_map.length;i++){
    if(binary_map[i] == ""){
      console.log(i)
      binary_map.splice(i,1)
    }
  }
  return binary_map;
}
function setBlocks(game, binary_map) {
  var k=0;
  for(var z=0;z<limit_z;z++){
    for(var y=0;y<limit_y;y++){
      for(var x=0;x<limit_x;x++){
        var value = parseInt(binary_map[k])
        if(value != 0){
          game.setBlock([x, y, z], value+1) // on
        }
        k++
      }
    }
  }

}

function defaultSetup(game, avatar) {
  var makeFly = fly(game)
  var target = game.controls.target()
  game.flyer = makeFly(target)

  //highlight blocks when you look at them, hold <Ctrl> for block placement
  // var blockPosPlace, blockPosErase
  // var hl = game.highlighter = highlight(game, { color: 0xff0000 })
  // hl.on('highlight', function (voxelPos) { blockPosErase = voxelPos })
  // hl.on('remove', function (voxelPos) { blockPosErase = null })
  // hl.on('highlight-adjacent', function (voxelPos) { blockPosPlace = voxelPos })
  // hl.on('remove-adjacent', function (voxelPos) { blockPosPlace = null })

  // toggle between first and third person modes
  window.addEventListener('keydown', function (ev) {
    if (ev.keyCode === 'R'.charCodeAt(0)) avatar.toggle()
    if (ev.keyCode === 'Y'.charCodeAt(0)) console.log(avatar.position)
    var makeFly = fly(game)
    var target = game.controls.target().avatar

    game.flyer = makeFly(target)
 
  })

  // block interaction stuff, uses highlight data
  var currentMaterial = 1

  // game.on('fire', function (target, state) {
  //   var position = blockPosPlace
  //   if (position) {
  //     game.createBlock(position, currentMaterial)
  //   }
  //   else {
  //     position = blockPosErase
  //     if (position) game.setBlock(position, 0)

  //     console.log(position)
  //   }
  // })

  game.on('tick', function() {
    //walk.render(target.playerSkin)
    // var vx = Math.abs(target.velocity.x)
    // var vz = Math.abs(target.velocity.z)
    // if (vx > 0.001 || vz > 0.001) walk.stopWalking()
    // else walk.startWalking()
  })

}
function to_hex_string(number) {
  var hex=number.toString(16);
  return new Array(3-hex.length).join(0) + hex
}

function HSVtoRGB(h, s, v) {
    var r, g, b, i, f, p, q, t;
    if (h && s === undefined && v === undefined) {
        s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    return {
        r: Math.floor(r * 255),
        g: Math.floor(g * 255),
        b: Math.floor(b * 255)
    };
}
